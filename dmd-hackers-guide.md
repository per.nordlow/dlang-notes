# The Hacker’s Guide to DMD

## Care about the call tree, potentially spanning multiple files rather than the files themselves and what they contain

## Motive

The motive of this document is to enable more developers to more quickly
understand the overall architecture of the dmd code base in order to be able to
improve it.

## Node Type Groups

- Symbol: `dsymbol.d`
- Expression: `expression.d`
- Statement: `statement.d`
- Template: `dtemplate.d`
- TODO add more

## Node Polymorphism

To maximize perforance, DMD implements AST node polymorphism in a hybrid way at
each node type group level.

### Node Casting

Node (dynamic) casting is realized

- manually as `if (node.tok == TOK.xxx) { cast(...)... }` or
- automatically as inline wrappers (member) functions
  - `{Expression,Statement,...}.is...{Expression,Statement,...}`.

## Semantic analysis happens in files matching `*sem.d`

- Symbol: `dsymbolsem.d`
- Initializer: `initsem.d`
- Expression: `expressionsem.d`
- Statement: `statementsem.d`
- Type: `typesem.d`
- Template: `templatesem.d`

## Semantic Analysis is divided into 3 Phases

- "semantic"
- "semantic2"
- "semantic3"

This model has been obseleted by Walter Bright as he as admitted that this
3-phase design was a mistake. Changing it is a big undertaking and will be with
us for the forseeable future.

## AST Visitors

The D compiler frontend supports the [visitor
pattern](https://en.wikipedia.org/wiki/Visitor_pattern) in various forms:

- `Visitor` is non-recursive. This is the most flexible visitor as classes
  overriding Visitor can choose ordering of visited sub-trees and if some
  sub-trees should be excluded in visiting.
- `TransitiveVisitor` is transitive (recursive) meaning it automatically calls
  visit() on _all_ the nodes in the AST pointed to by the tree root node fed to
  the visitor constructor.
- `ParseTimeTransitiveVisitor` is _supposed_ to be run after (or during?)
  parsing before semantic analysis has been performed.
- `SemanticTimeTransitiveVisitor` is _supposed_ to be run after semantic
   analysis has been performed. See
   https://dlang.slack.com/archives/C1ZDHBB2S/p1620825584111800.
- `StoppableVisitor` with member `bool stop`.
- Every visitor inheriting from the above should, when possible, be declared
  `final` to avoid virtual class to its `visit()` methods.
- Every AST-node class should, when possible, be declared `final` to avoid
  virtual class to its `accept()` methods.
- Node `accept()` and Visito `visit()` are inferred `final` because their
 parenting classes are declared `final`.

New more advanced diagnostics are preferrably realized by via a subclass of an
existing visitor, usually the `SemanticTimeTransitiveVisitor` as that class
contains the most detailed (type) information. Such advanced diagnostics are
preferrably triggered _after_ an error has occurred as that doesn’t incur any
performance cost for the normal code path taken when checking or compilation
succeeds.

An automatic warning or inference of `final` for non-`final` classes that has
non-derived-classes could be written.

## Why is `Scope` a `struct` and not a `class`?

The `Scope` `struct` is not part of any class hierarchy and its instances are
constructed and destructed deterministically and sequentially as a stack on top
of a memory region. Therefore no reliance on the GC is needed and as there
currently is only one kind of scope it’s implemented as a `struct`.

## Lowering

- Related `Dsymbol`s are often tagged so `isAnonymous` returns `true`.
- Generated AST nodes are tagged so `isGenerated` return `true`.

## Special Symbol Singletons

- `Id.postblit` is the explicit postblit.
- `Id.__xpostblit` is the compiler-generated postblit.

## DMD real entry point: `tryMain` in file `mars.d`

## AST Duplication

AST duplication is performed via `syntaxCopy`.

## Enums

Conversion from an `enum` to its base type via `toBaseType` is used extensively.

## AST printing and to-text-conversino

- `kind()` shows `import` for imports, `class` etc
- `toChars()`
- `toString()`
- `astTypeName()`
- `message()`

## Expressions, Statements, Functions

## Casting

- Casting at different levels
- Supports both non-virtual and virtual

- `dyncast`
- `DYNCAST`

- `RootObject`:

Non-virtual by switching on `Expression.op`:
- `Expression`: is...Expr

## Expressions ('expression.d`):

- `AndExp`: `x & y`

- `OrExp`: `x | y`

- `LogicalExp`: `x && y` and `x || y`.  Should be split into `AndAndExp` and
 `OrOrExp` for consistency `TOK.andAnd` and `TOK.orOr`.

- `isVarExp`, `isDotVarExp`, etc are all `final` members of `Expression` and
  therefore as fast as the unsafer `... == TOK...`. Use them whenever you can
  when doing AST sub-tree matching.

- Semantic analysis in `expressionsem.d`.

- `isType...`
- `is...Exp`
- `VarExp` -> `SymbolExp`
- `DsymbolExp`
- `IdentifierExp`

- `callCpCtor`
- `doCopyOrMove`

- `Scope`

- `IndexExp`: AAs,
- `AssignExp`: AAs, prevent postblit from being called when argument is an r-value

- `callCpCtor`

- `isLvalue`

- `doCopyOrMove`

- `valueNoDtor`

- `STCnodtor`
- `STCrvalue`

- `isVarDecl`

## Central: Semantic Analysis of Expression ('expressionsem.d`)

## Expression Types

- `isVarExp()`: `x`
- `isPtrExpr()`: `*x`
- `isSymOffExp()`: `&x`
- `isDotVarExp`: `this.x`

## Casting predicates between node types is found in `expression.d`

## Functions and function calls:

- Call Expression: `CallExp`
- FuncDeclaration: `CallExp`
- Use https://github.com/dlang/dmd/pull/6148/files
- Detect direct infinite self-recursion: Check if `CallExp.fname` equals current
  function parameters unchanged

- Import: `dimport.d`, `Import::search`, `ScopeDsymbol.importedScopes`

- Other: `ScopeDsymbol.prots`

- Lookup symbol:
  - `::search_correct(`
  - `::search(`

- Symbol Reference Expressions
  - `SymbolExp`
  - `VarExp`
  - `SymOffExp`
  - `DsymbolExp`

## Templates

When a template (`TemplateDeclaration`) is instantiated multiple times all those
template instances get linked together via `TemplateInstance.tnext`.

`TemplateInstance`s "sit beside" the normal AST of
`Declaration`s. `TemplateInstance`s have their own instance tree (upwards
traversable via the `TemplateInstance.tinst` field).

The code of the function template instance is represented by a normal
`FuncDeclaration` fd) where the `TemplateInstance` is reachable via the
`fd.parent`. The `parent` field is a member of `Dsymbol` that `FuncDeclaration`
inherits from.
