# The Future: A Distributed Permanant Load-Balanced Internet/Web

## Theory!

- This is "New not News"
  - Hard to understand!?
  - Hard to believe!?

## What Internet should have been and will be

- Content-Based Addressing (SHA1)
  - Block-Level => Differential Compression for free (like Git)

- What Internet should have been

- Less stress on centralized servers:
  - YouTube: "Gangname style" downloaded milions of time from the same Google server.

- Merkle DAG

- Content-Based Dependencies
  - Erlang's Joe Armstrongs talk "The Mess We're in":
    "A dependency is just a blob with a hash"
  - Build Systems
    - SCons Cache: ~/.scons/cache/00/...
    - Stallmans: GUIX

- More DoS Attack Resilient: TODO Why?

- Permanence (like Git): No more dead links!

- Company-local (such as Facebook) links are not scalable. If user account is
  deleted the link will be dead.

## [MaidSafe](http://maidsafe.net/)

## IPFS

- [IPFS](https://ipfs.io/): [Why?](https://ipfs.io/#why):
  - Git, Bittorrent, ... combined
  - Replaces HTTP

- Licklider's "The Intergalactic Network" became Internet

- Authentication

- Protocol: Securer than most others because: The files themselves (both yours
  but also others) on disk storage are encrypted (rather than in the
  communication link). TODO How are hashes related?

- IPFS is the most ready solution

- Protocol Implementations: Go, Javascript, Python

- Like ordinary consumer its our reponsibility as developers: Its up to us!

- Changes the rules of the Internet especially with regards to public vs company closed

- Automatic Caching => (Web) Apps can run offline

## Comparisons:

- [MaidSafe vs IPFS](https://www.reddit.com/r/ipfs/comments/4mh51s/maidsafe_vs_ipfs/)

## Workshop and Practise!
